# Laravel Datatable 

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. 

## Installation



```bash
git clone https://gitlab.com/buhahemal10/laravel_datatable.git
```
Open Your Terminal And Go To Project Directory Run Below Command:
```bash
composer install
```
```bash
cp .env.example .env
```
```bash
php artisan key:generate
```
after Key generate open .env file and change database crenditials

```bash
php artisan migrate 
```
it will create 1000 dummy record if you want more just change number inside Databaseseeder File.
```bash
PHP artisan db:seed
```
Thank You 
